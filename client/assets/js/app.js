(function() {
  'use strict';

  angular.module('application', [
    'ui.router',
    'ngAnimate',

    //foundation
    'foundation',
    'foundation.dynamicRouting',
    'foundation.dynamicRouting.animations'
  ])
    .controller('mainCtrl', function($scope, $state, $http) {

      $scope.menhu = function(){
       
        $(".panel").addClass("showPanel");
        $(".menu3").delay(500).addClass("showMenu");
        
        $(".panel").click(function(){
            $(".menu3").removeClass("showMenu");
            $(".panel").delay(500).removeClass("showPanel");
        });
      }

      $(document).ready(function(){
         var height = $('#tubo1').width();
         $('#tubo1').css({'height':height+'px'});
         
         var height = $('#tubo2').width();
         $('#tubo2').css({'height':height+'px'});
         
         var height = $('#tubo3').width();
         $('#tubo3').css({'height':height+'px'});
         
         var height = $('#tubo4').width();
         $('#tubo4').css({'height':height+'px'});
         
         var height = $('#tubo5').width();
         $('#tubo5').css({'height':height+'px'});
         
         var height = $('#tubo6').width();
         $('#tubo6').css({'height':height+'px'});
         
         var height = $('#tubo7').width();
         $('#tubo7').css({'height':height+'px'});
         
         var height = $('#tubo8').width();
         $('#tubo8').css({'height':height+'px'});
      });

      $(window).resize(function(){
         var height = $('#tubo1').width();
         $('#tubo1').css({'height':height+'px'});
         
         var height = $('#tubo2').width();
         $('#tubo2').css({'height':height+'px'});
         
         var height = $('#tubo3').width();
         $('#tubo3').css({'height':height+'px'});
         
         var height = $('#tubo4').width();
         $('#tubo4').css({'height':height+'px'});
         
         var height = $('#tubo5').width();
         $('#tubo5').css({'height':height+'px'});
         
         var height = $('#tubo6').width();
         $('#tubo6').css({'height':height+'px'});
         
         var height = $('#tubo7').width();
         $('#tubo7').css({'height':height+'px'});
         
         var height = $('#tubo8').width();
         $('#tubo8').css({'height':height+'px'});
      });

      $scope.url = './assets/sendemail.php';
      $scope.loading = ""

      $scope.embed = function(url) {
        $scope.selectedpdf = "url"
      }

      $scope.formsubmit2 = function() {
          $scope.loading = "true"
          $http({
              method: 'POST',
              url: './assets/sendemail.php',
              data: 'name=' + $scope.name2 + '&email=' + $scope.email2 + '&asunto=' + 'contacto' + '&message=' + $scope.message2,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          }).
          success(function(data, status) {
              console.log(data);
              $scope.status2 = status;
              $scope.data2 = data;
              $scope.result2 = data;
              $scope.loading = ""
          }).
          error(function(data, status) {
              alert("Intenta de nuevo");
              $scope.loading = ""
          })
      }

    })
    .controller('contactoCtrl', function($scope, $state, $http) {

      var map;
      var myLatLng = {lat: 25.6965256, lng: -100.2563343};
      var ofice = {lat: 25.646786, lng: -100.3240982};
      var planta = {lat: 25.743621, lng: -100.1851837};
      map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 11,
          gestureHandling: 'cooperative',
          scrollwheel:  false
      });
      var image = './assets/img/spazio_pin.png';
      var marker = new google.maps.Marker({
        map: map,
        /*icon: image,*/
        position: ofice,
        title: 'Oficina'
      });

      var marker2 = new google.maps.Marker({
        map: map,
        /*icon: image,*/
        position: planta,
        title: 'Planta'
      });
      var styles = [
        {
          stylers: [
            { hue: "#003A5D" },
            { saturation: -20 }
          ]
        },{
          featureType: "road",
          elementType: "geometry",
          stylers: [
            { lightness: 100 },
            { visibility: "simplified" }
          ]
        },{
          featureType: "road",
          elementType: "labels",
          stylers: [
            { visibility: "off" }
          ]
        }
      ];
      map.setOptions({styles: styles});
      $scope.menhu = function(){
        
        $(".panel").addClass("showPanel");
        $(".menu3").delay(500).addClass("showMenu");

        $(".panel").click(function(){
            $(".menu3").removeClass("showMenu");
            $(".panel").delay(500).removeClass("showPanel");
        });
      }


      $scope.url = './assets/sendemail.php';

      $scope.formsubmit = function() {
          $http({
              method: 'POST',
              url: './assets/sendemail.php',
              data: 'name=' + $scope.name + '&email=' + $scope.email + '&asunto=' + 'contacto' + '&message=' + $scope.message,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          }).
          success(function(data, status) {
              console.log(data);
              $scope.status = status;
              $scope.data = data;
              $scope.result = data;
          }).
          error(function(data, status) {
              alert(data + " Gracias!");
          })
      }


    })

    .config(config)
    .run(run)
  ;

  config.$inject = ['$urlRouterProvider', '$locationProvider'];

  function config($urlProvider, $locationProvider) {
    $urlProvider.otherwise('/');

    $locationProvider.html5Mode({
      enabled:false,
      requireBase: false
    });

    $locationProvider.hashPrefix('!');
  }

  function run() {
    FastClick.attach(document.body);
  }

})();
