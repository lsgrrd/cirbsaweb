<?php
require_once('phpmailer/PHPMailerAutoload.php');

$toemails = array();

$toemails[] = array(
				'email' => 'josemanueltrevino@cirbsa.com',
				'name' => 'Cirbsa'
			);

// Form Processing Messages
$message_success = 'Gracias por contactarnos';

// Add this only if you use reCaptcha with your Contact Forms
$recaptcha_secret = ''; // Your reCaptcha Secret

$mail = new PHPMailer();


// If you intend you use SMTP, add your SMTP Code after this Line
$mail->IsSMTP();
$mail->Host = "smtp.webfaction.com";
/*$mail->SMTPDebug = 2;*/
$mail->SMTPAuth = true;
$mail->Port = 25;
$mail->Username = "mailserver";
$mail->Password = "superSeguridad";


if( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
	if( $_POST['email'] != '' ) {

		$name = isset( $_POST['name'] ) ? $_POST['name'] : '';
		$email = isset( $_POST['email'] ) ? $_POST['email'] : '';
		$asunto = isset( $_POST['asunto'] ) ? $_POST['asunto'] : '';
		$message = isset( $_POST['message'] ) ? $_POST['message'] : '';
		
		$subject = 'New Message From Contact Form';

		//$botcheck = $_POST['template-contactform-botcheck'];

		if( $botcheck == '' ) {

			$mail->SetFrom( "website@cirbsa.com" , $name );
			$mail->AddReplyTo( $email , $name );
			foreach( $toemails as $toemail ) {
				$mail->AddAddress( $toemail['email'] , $toemail['name'] );
			}
			$mail->Subject = $subject;

			$name = isset($name) ? "Nombre: $name<br><br>" : '';
			$email = isset($email) ? "Email: $email<br><br>" : '';
			$asunto = isset($asunto) ? "Asunto: $asunto<br><br>" : '';
			$message = isset($message) ? "Mensaje: $message<br><br>" : '';

			$referrer = $_SERVER['HTTP_REFERER'] ? '<br><br><br>This Form was submitted from: ' . $_SERVER['HTTP_REFERER'] : '';

			$body = "$name $email $asunto $message $referrer";

			// Runs only when File Field is present in the Contact Form
			if ( isset( $_FILES['template-contactform-file'] ) && $_FILES['template-contactform-file']['error'] == UPLOAD_ERR_OK ) {
				$mail->IsHTML(true);
				$mail->AddAttachment( $_FILES['template-contactform-file']['tmp_name'], $_FILES['template-contactform-file']['name'] );
			}

			// Runs only when reCaptcha is present in the Contact Form
			if( isset( $_POST['g-recaptcha-response'] ) ) {
				$recaptcha_response = $_POST['g-recaptcha-response'];
				$response = file_get_contents( "https://www.google.com/recaptcha/api/siteverify?secret=" . $recaptcha_secret . "&response=" . $recaptcha_response );

				$g_response = json_decode( $response );

				if ( $g_response->success !== true ) {
					echo 'Captcha not Validated! Please Try Again';
					die;
				}
			}

			$mail->MsgHTML( $body );
			$sendEmail = $mail->Send();

			if( $sendEmail == true ){
				echo 'We have successfully received your Message and will get Back to you as soon as possible.';
			}
			else{
				echo 'Email could not be sent due to some Unexpected Error. Please Try Again later.';
			}
		} else {
			echo 'Bot Detected! Clean yourself Botster!';
		}
	} else {
		echo 'Please Fill up all the Fields and Try Again.';
	}
} else {
	echo 'An unexpected error occured. Please Try Again later.';
}

?>